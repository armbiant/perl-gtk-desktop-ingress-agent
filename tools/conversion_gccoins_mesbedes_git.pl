#!/usr/bin/perl
use strict;
use warnings;
use POSIX;
# permet le tri avec prise en compte des accents et des majuscules
use Unicode::Collate;
my $col = Unicode::Collate->new(level => 1);


# outil de conversion d'une collection de monnaie GCstar GCcoins de MesBedes (2015) vers modèle GIT 
# format XML.
# en entrée, le nom du fichier gcstar et de son chemin
# en sortie, le même fichier réécrit

# objectifs : 
# 1) convertir le champs Front en Picture
# 2) convertir le champs Estimate1 en Estimate
# 3) ajouter les champs Edge2, Edge3 et Edge4


# pour bien manipuler les caractères exotiques (en entrée et en sortie)
use open(IO => ':encoding(utf8)');
binmode(STDERR, ':utf8');
binmode(STDOUT, ':utf8');
binmode(STDIN, ':utf8');

# le fichier GCcoins contenant la collection de monnaie GCstar à analyser
my $fichier = "numismatiq.gcs";


# pour manipuler un fichier XML
# utilisation du module LibXML
use XML::LibXML;
my $parser = XML::LibXML->new();
# création de l'arbre XML en fonction du fichier lu
my $tree = $parser->parse_file($fichier);

# Racine du document XML
my $root = $tree->getDocumentElement;

# la balise collection correspond à la racine du fichier XML
# le type de collection dans la balise collection
# le type doit correspondre à une collection GCcoins
my $data_type = $root->getAttribute('type');

# si ce n'est pas le bon type, il vaut mieux tout arrêter
if ($data_type ne "GCcoins" ) {
	exit;
}

# le nombre d'items (de monnaie) dans la balise collection 
my $data_items = $root->getAttribute('items');

# la version de la collection GCstar dans la balise collection
my $data_version = $root->getAttribute('version');

# le numéro maximum attribué (pour la création du prochain item)
my $data_maxId;

  # Balise information
  foreach my $information ( $root->getElementsByTagName('information') ) {
	# Balise maxId
	foreach my $maxId ( $information->getElementsByTagName('maxId') ) {
		$data_maxId = $maxId->getFirstChild->getData;
	}
  }

# Création du fichier résultat en UTF8 (?)
# il est sensé être la reproduction fidèle du fichier en entrée
# avec des corrections si possible
# faire une comparaison avec MELD
my $FichierResultat = 'monnaies.gcs';


open( my $FhResultat, '>', $FichierResultat )
  or die("Impossible d'ouvrir le fichier $FichierResultat\n$!");

print {$FhResultat} '<?xml version="1.0" encoding="UTF-8"?>'."\n";
print {$FhResultat} '<collection type="'.$data_type.'" items="'.$data_items.'" version="'.$data_version.'">'."\n";
print {$FhResultat} ' <information>'."\n";
print {$FhResultat} '  <maxId>'.$data_items.'</maxId>'."\n";
print {$FhResultat} ' </information>'."\n";
# pourquoi 3 lignes vides ?
print {$FhResultat} ''."\n";
print {$FhResultat} ''."\n";
print {$FhResultat} ''."\n";

# identifiant d'une pièce
my $id = 0;
my $nb_items = 0;

# Balise item (monnaie)
my @item = $root->getElementsByTagName('item');

# pour voir le contenu du document XML chargé
#use Data::Dumper;
#print Dumper @item;

# ici le tri ?
# todo : tri par pays puis par nom de @item

  # parcours des attributs de chaque item
  foreach my $monnaie ( @item ) {
	 my $data_gcsautoid = $monnaie->getAttribute('gcsautoid');
	 my $data_name = $monnaie->getAttribute('name');
	 my $data_front = $monnaie->getAttribute('front');
	 my $data_back = $monnaie->getAttribute('back');
	 my $data_country = $monnaie->getAttribute('country');
	 my $data_year = $monnaie->getAttribute('year');
	 my $data_quantity = $monnaie->getAttribute('quantity');
	 my $data_years_of_coinage = $monnaie->getAttribute('years_of_coinage');
	 my $data_value = $monnaie->getAttribute('value');
	 my $data_currency = $monnaie->getAttribute('currency');
	 my $data_type = $monnaie->getAttribute('type');
	 my $data_mint = $monnaie->getAttribute('mint');
	 my $data_mint_mark = $monnaie->getAttribute('mint_mark');
	 my $data_mintmaster = $monnaie->getAttribute('mintmaster');
	 my $data_mintmaster_mark = $monnaie->getAttribute('mintmaster_mark');
	 my $data_city = $monnaie->getAttribute('city');
	 my $data_city_letter = $monnaie->getAttribute('city_letter');
	 my $data_series = $monnaie->getAttribute('series');
	 my $data_metal = $monnaie->getAttribute('metal');
	 my $data_weight = $monnaie->getAttribute('weight');
	 my $data_diameter = $monnaie->getAttribute('diameter');
	 my $data_depth = $monnaie->getAttribute('depth');
	 my $data_number = $monnaie->getAttribute('number');
	 my $data_form = $monnaie->getAttribute('form');
	 my $data_axis = $monnaie->getAttribute('axis');
	 my $data_edge_type = $monnaie->getAttribute('edge_type');
	 my $data_edge1 = $monnaie->getAttribute('edge1');
	 my $data_condition = $monnaie->getAttribute('condition');
	 my $data_buyed = $monnaie->getAttribute('buyed');
	 my $data_found = $monnaie->getAttribute('found');
	 my $data_bring = $monnaie->getAttribute('bring');
	 my $data_gift = $monnaie->getAttribute('gift');
	 my $data_location = $monnaie->getAttribute('location');
	 my $data_added = $monnaie->getAttribute('added');
	 my $data_catalogue1 = $monnaie->getAttribute('catalogue1');
	 my $data_number1 = $monnaie->getAttribute('number1');
	 my $data_estimate1 = $monnaie->getAttribute('estimate1');
	 my $data_catalogue2 = $monnaie->getAttribute('catalogue2');
	 my $data_number2 = $monnaie->getAttribute('number2');
	 my $data_estimate2 = $monnaie->getAttribute('estimate2');
	 my $data_website = $monnaie->getAttribute('website');
	 my $data_webPage = $monnaie->getAttribute('webPage');
	 my $data_price = $monnaie->getAttribute('price');
	 my $data_favourite = $monnaie->getAttribute('favourite');

	# nouveaux champs
     my $data_edge2 = "";
     my $data_edge3 = "";
     my $data_edge4 = "";
     
	 my $data_tags1 = "";
	if ($monnaie->getAttribute('tags')) {
		$data_tags1 = $monnaie->getAttribute('tags');
	}

	my $data_head = "";
	# balise head
	foreach my $head ( $monnaie->getElementsByTagName('head') ) {
		# attention quand la balise est vide
		if ($head->hasChildNodes) { 
			$data_head = $head->getFirstChild->getData;
		}
	}

	my $data_tail = "";
	# balise tail
	foreach my $tail ( $monnaie->getElementsByTagName('tail') ) {
		if ($tail->hasChildNodes) { 
			$data_tail = $tail->getFirstChild->getData;
		}
	}

	my $data_edge = "";
	# balise edge
	foreach my $edge ( $monnaie->getElementsByTagName('edge') ) {
		if ($edge->hasChildNodes) { 
			$data_edge = $edge->getFirstChild->getData;
		}
	}

	my $data_comments = "";
	# balise comments
	foreach my $comment ( $monnaie->getElementsByTagName('comments') ) {
		if ($comment->hasChildNodes) { 
			$data_comments = $comment->getFirstChild->getData;
		}
	}

	my $data_history = "";
	# balise history
	foreach my $history ( $monnaie->getElementsByTagName('history') ) {
		if ($history->hasChildNodes) { 
			$data_history = $history->getFirstChild->getData;
		}
	}

	my $data_references = "";
	# balise references
	foreach my $references ( $monnaie->getElementsByTagName('references') ) {
		if ($references->hasChildNodes) { 
			$data_references = $references->getFirstChild->getData;
		}
	}

	# en fin de fichier les tags sont gérés avec une balise
	my $data_tags2 = "";
	if ($monnaie->getElementsByTagName('tags')) {
		# balise tags
		foreach my $tags ( $monnaie->getElementsByTagName('tags') ) {
			if ($tags->hasChildNodes) {
				foreach my $line ( $tags->getElementsByTagName('line') ) {
					if ($line->hasChildNodes) {
						foreach my $col ( $line->getElementsByTagName('col') ) {
							$data_tags2 = $col->getFirstChild->getData;
						}
					}
				}
			}
		}
	}

# comptage des monnaies
$nb_items += 1;

# écriture de la monnaie dans le fichier résultat
# renuméroter de 1 à $data_items
$id++;
print {$FhResultat} ' <item'."\n";
print {$FhResultat} '  gcsautoid="'.$id.'"'."\n";
print {$FhResultat} '  name="'.&xml_quote($data_name).'"'."\n";

# changement du champs Front
print {$FhResultat} '  picture="'.$data_front.'"'."\n";

print {$FhResultat} '  back="'.$data_back.'"'."\n";
print {$FhResultat} '  country="'.&xml_quote($data_country).'"'."\n";
print {$FhResultat} '  year="'.$data_year.'"'."\n";
print {$FhResultat} '  quantity="'.$data_quantity.'"'."\n";
print {$FhResultat} '  years_of_coinage="'.$data_years_of_coinage.'"'."\n";
print {$FhResultat} '  value="'.$data_value.'"'."\n";
print {$FhResultat} '  currency="'.&xml_quote($data_currency).'"'."\n";
print {$FhResultat} '  type="'.$data_type.'"'."\n";
print {$FhResultat} '  mint="'.$data_mint.'"'."\n";
print {$FhResultat} '  mint_mark="'.$data_mint_mark.'"'."\n";
print {$FhResultat} '  mintmaster="'.&xml_quote($data_mintmaster).'"'."\n";
print {$FhResultat} '  mintmaster_mark="'.$data_mintmaster_mark.'"'."\n";
print {$FhResultat} '  city="'.$data_city.'"'."\n";
print {$FhResultat} '  city_letter="'.$data_city_letter.'"'."\n";
print {$FhResultat} '  series="'.&xml_quote($data_series).'"'."\n";
print {$FhResultat} '  metal="'.&xml_quote($data_metal).'"'."\n";
print {$FhResultat} '  weight="'.$data_weight.'"'."\n";
print {$FhResultat} '  diameter="'.$data_diameter.'"'."\n";
print {$FhResultat} '  depth="'.$data_depth.'"'."\n";
print {$FhResultat} '  number="'.$data_number.'"'."\n";
print {$FhResultat} '  form="'.$data_form.'"'."\n";
print {$FhResultat} '  axis="'.$data_axis.'"'."\n";
print {$FhResultat} '  edge_type="'.&xml_quote($data_edge_type).'"'."\n";
print {$FhResultat} '  edge1="'.$data_edge1.'"'."\n";

# nouveaux champs image
print {$FhResultat} '  edge2="'.$data_edge2.'"'."\n";
print {$FhResultat} '  edge3="'.$data_edge3.'"'."\n";
print {$FhResultat} '  edge4="'.$data_edge4.'"'."\n";

print {$FhResultat} '  condition="'.$data_condition.'"'."\n";
print {$FhResultat} '  buyed="'.$data_buyed.'"'."\n";
print {$FhResultat} '  found="'.$data_found.'"'."\n";
print {$FhResultat} '  bring="'.$data_bring.'"'."\n";
print {$FhResultat} '  gift="'.$data_gift.'"'."\n";
print {$FhResultat} '  location="'.&xml_quote($data_location).'"'."\n";
print {$FhResultat} '  added="'.$data_added.'"'."\n";
print {$FhResultat} '  catalogue1="'.$data_catalogue1.'"'."\n";
print {$FhResultat} '  number1="'.$data_number1.'"'."\n";

# changement du champs Estimate1
print {$FhResultat} '  estimate="'.$data_estimate1.'"'."\n";

print {$FhResultat} '  catalogue2="'.$data_catalogue2.'"'."\n";
print {$FhResultat} '  number2="'.$data_number2.'"'."\n";
print {$FhResultat} '  estimate2="'.$data_estimate2.'"'."\n";
print {$FhResultat} '  website="'.&xml_quote($data_website).'"'."\n";
print {$FhResultat} '  webPage="'.&xml_quote($data_webPage).'"'."\n";
print {$FhResultat} '  price="'.$data_price.'"'."\n";
print {$FhResultat} '  favourite="'.$data_favourite.'"'."\n";

print {$FhResultat} ' >'."\n";

print {$FhResultat} '  <head>'.&xml_quote($data_head).'</head>'."\n";
print {$FhResultat} '  <tail>'.&xml_quote($data_tail).'</tail>'."\n";
print {$FhResultat} '  <edge>'.&xml_quote($data_edge).'</edge>'."\n";
print {$FhResultat} '  <comments>'.&xml_quote($data_comments).'</comments>'."\n";
print {$FhResultat} '  <history>'.&xml_quote($data_history).'</history>'."\n";
print {$FhResultat} '  <references>'.&xml_quote($data_references).'</references>'."\n";

print {$FhResultat} '  <tags>';
if ($data_tags2 or $data_tags1) {
	print {$FhResultat} '<line>';
	print {$FhResultat} '<col>'.&xml_quote($data_tags1).&xml_quote($data_tags2).'</col>';
	print {$FhResultat} '</line>';
}
print {$FhResultat} '</tags>'."\n";
print {$FhResultat} ' </item>'."\n";

	# fin items
  }

# fin du fichier résultat
print {$FhResultat} '</collection>'."\n";
close($FhResultat);


sub ajout_zero {
	my(@args) = @_;
	my $t = $args[0];
	$t = sprintf ("%0*d", 2, $t);
	return($t) ;
}

sub xml_quote {
	my(@args) = @_;
	my $t = $args[0];
	# permet de remplacer les & par des &amp; les < et les > aussi ainsi que les "
	# toujours commencer par le &
	$t =~ s/&/&amp;/gm;
	$t =~ s/</&lt;/gm;
	$t =~ s/>/&gt;/gm;
	$t =~ s/"/&quot;/gm;
	
	return($t) ;
}

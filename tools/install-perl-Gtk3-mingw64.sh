#! /usr/bin/bash
#
# script to build GCstar on Windows/MSYS2/Mingw64 environment
# 
# steps: 
#    msys2_packages : all binary packages
#    perl_dependencies : generic Perl modules to build and run GCstar
#    optional_dependencies : for a more functional GCstar
#    optional_GD_dependencies : to add statistics
#

install_step=$1

script_dir=Gtk3_Build
make_cmd="mingw32-make"
make_options="SHELL=/usr/bin/sh"

mkdir $script_dir || echo "$script_dir exists"

cd $script_dir

#########################
# Build functions
#########################
install_perl_module () {

	module=$1
	hack_function=$2
	shift
	shift

	echo "*--------------------------"
	echo "* INSTALLING $module "
	echo "*--------------------------"
	
	info=`cpanm --info $module`
	link=$(get_link $info)
	filename=${info/*\///}
	dirname=$filename
	dirname=${dirname/.tar.gz//}

	echo "Download $module from $link"
	test -f ".$filename" || wget -q $link 

	echo "Extract $module"
 	tar xzf ".$filename"

	cd ".$dirname"
	$hack_function $*

	echo "Building $module"
	$make_cmd $make_options

	echo "Building static $module"
	$make_cmd static $make_options
		
	echo "Installing $module"
	$make_cmd install $make_options

	cd ..
}

no_hack () {
    perl ./Makefile.PL
}

hack_makefile () {
    #
	# function to adapt the Perl/Gtk3 installation process to MSYS2
	#
	# goal :
	#      patch the libraries refernces to prevent linking errors
	#
	# sources :
	#      https://github.com/MaxPerl/perl-Gtk3-windows-installer
	#      https://github.com/orbital-transfer-example/perl-gtk3-starter-basic
	#      https://project-renard.github.io/doc/development/meeting-log/posts/2016/05/03/windows-build-with-msys2/
    
	perl Makefile.PL verbose $* > hack.log

	grep " LIBS =>" hack.log | sed 's/ *LIBS => \[*q\[*//' | sed 's/\]//g' | \
	    sed 's/-lGlib/-l:Glib.dll/' | sed 's/-lCairo/-l:Cairo.dll/' |\
	    #sed 's/libgd.a/libgd.dll.a/' | \
        sed 's/-lgd/-lgd/' > libs.log

	libs=`sed 's/^/:nosearch /' libs.log`
	echo "======= LIBS $libs"
	cp Makefile Makefile_nolibs_$RANDOM
	perl ./Makefile.PL $* LIBS="$libs"
	cp Makefile Makefile_libs_$RANDOM
}

fix_makefile () {
	#
	# function to fix the Makefile for some packages : Net::FreeDB
	#

	sed "/'DEFINE'/s/''/'-DPACKED='/" < Makefile.PL > tmp.PL
	mv tmp.PL Makefile.PL 
	perl ./Makefile.PL $* 
	cp Makefile Makefile_libs_$RANDOM
}

get_link () {
	info=$1
	firstletter=${info:0:1}
	firsttwoletters=${info:0:2}
    #result="get_link $info $firstletter $firsttwoletters"
	echo "https://cpan.metacpan.org/authors/id/$firstletter/$firsttwoletters/$info"
}

print_welcome_page () {
	echo "Perl/Gtk3/GCstar Installer for Mingw64/MSys2 on Windows"
	echo "--------------------------------------------------------------"
}

print_welcome_page

install_msys2_packages() {
  echo "-----------------------------------------"
  echo "- INSTALLING THE BUILD TOOLCHAIN "
  echo "-----------------------------------------"
  for p in pkgconf binutils gcc make perl
  do
    pacman -S --needed --noconfirm --noprogressbar mingw-w64-x86_64-$p
  done

  echo "-----------------------------------------"
  echo "- INSTALLING NATIVE DEPENDENCIES "
  echo "-----------------------------------------"
  for p in gobject-introspection cairo gtk3
  do
    pacman -S --needed --noconfirm --noprogressbar mingw-w64-x86_64-$p
  done
}

install_perl_dependencies() {
  echo "-----------------------------------------"
  echo "- INSTALLING PERL DEPENDENCIES "
  echo "-----------------------------------------"

  echo "Installing App::cpanminus"

  curl -s -L https://cpanmin.us > cpan.tar.gz
  perl - App::cpanminus < cpan.tar.gz

  cpanm -n 'ExtUtils::Depends'
  cpanm -n 'ExtUtils::PkgConfig'

  for m in Date::Calc LWP::Simple
  do
    cpanm -n "$m"
  done

  # installation ask if external tests need to be performed
  yes n | cpanm -n "LWP::Protocol::https"

  for m in 'Glib' 'Cairo' 'Cairo' 'Glib::Object::Introspection' 'Cairo::GObject'
  do
    install_perl_module $m hack_makefile
  done

  install_perl_module 'Gtk3' no_hack
  install_perl_module 'Gtk3::SimpleList' no_hack
}

install_optional_dependencies() {

  for m in Archive::Zip Date::Calc \
           MP3::Info MP3::Tag \
	       Image::ExifTool XML::Simple JSON Locale::Codes \
		   Ogg::Vorbis::Header::PurePerl CDDB::File Moo
  do
    cpanm -n "$m"
  done

  install_perl_module 'Net::FreeDB' fix_makefile
  
  # Todo : remove unused dependencies 
  for m in DateTime::Format::Strptime
  do
    cpanm -n "$m"
  done
}

install_custom_libgd() {

	# libgd with minimal images formats (jpeg, png)

	mkdir mingw-w64-libgd

	cd mingw-w64-libgd
	wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libgd/PKGBUILD
	wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libgd/libgd-export-pkg-config-build-in-subdirs.patch
	wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libgd/mingw-getline-link.patch
	wget https://raw.githubusercontent.com/msys2/MINGW-packages/master/mingw-w64-libgd/mingw-replace-posix_memalign.patch 

	grep -v libtiff PKGBUILD | grep -v libavif | grep -v libheif | grep -v libwebp | grep -v xpm-nox |\
		sed 's/_AVIF=ON/_AVIF=OFF/' | sed 's/_HEIF=ON/_HEIF=OFF/' | sed 's/_XPM=ON/_XPM=OFF/' | \
		sed 's/_TIFF=ON/_TIFF=OFF/' | sed 's/_WEBP=ON/_WEBP=OFF/' > tmp.txt

	mv tmp.txt PKGBUILD

	pacman  -S --needed --noconfirm --noprogressbar patch
	MINGW_ARCH=mingw64 makepkg-mingw --noconfirm --needed --noprogressbar -sLf
	pacman -U --needed --noconfirm --noprogressbar *.pkg.tar.zst
}

install_optional_GD_dependencies() {

  install_perl_module 'GD' hack_makefile --lib_gd_path /mingw64/lib  --options "PNG"

  # prevent interactivity in JSON module installation
  export PERL_CANARY_STABILITY_NOPROMPT=1 # for JSON

  for m in GD::Text GD::Graph GD::Image GD::Simple
  do
    cpanm -n "$m"
  done
}

cat > ../list_dll_dependencies.txt <<EOFEOFEOF
imagequant.dll
libLerc.dll
libatk-1.0-0.dll
libbrotlicommon.dll
libbrotlidec.dll
libbz2-1.dll
libcairo-2.dll
libcairo-gobject-2.dll
libcairo-script-interpreter-2.dll
libcharset-1.dll
libcrypto-*.dll
libdatrie-1.dll
libdeflate.dll
libepoxy-0.dll
libexpat-1.dll
libfeature_support.dll
libffi-8.dll
libfontconfig-1.dll
libformw6.dll
libfreetype-6.dll
libfribidi-0.dll
libgcc_s_seh-1.dll
libgd.dll
libgdk-3-0.dll
libgdk_pixbuf-2.0-0.dll
libgio-2.0-0.dll
libgirepository-1.0-1.dll
libglib-2.0-0.dll
libgmodule-2.0-0.dll
libgobject-2.0-0.dll
libgraphite2.dll
libgtk-3-0.dll
libharfbuzz-0.dll
libiconv-2.dll
libintl-8.dll
libjbig-0.dll
libjpeg-8.dll
liblzma-5.dll
libpango-1.0-0.dll
libpangocairo-1.0-0.dll
libpangoft2-1.0-0.dll
libpangowin32-1.0-0.dll
libpcre2-8-0.dll
libpixman-1-0.dll
libpng16-16.dll
libssl-*-x64.dll
libstdc++-6.dll
libthai-0.dll
libwinpthread-1.dll
libzstd.dll
perl532.dll
zlib1.dll
EOFEOFEOF

remove_unused_files() {
	find . -name '*.pod' -exec rm -f {} \;
	find . -name '*.a' | grep -v dll | while read f
	do
		rm $f
	done
	find . -name '*.h' -exec rm -f {} \;
	rm -r -f perl5/site_perl/5.32.1/MSWin32-x64-multi-thread/.meta
	for f in App CPAN perl5db.pl Pod TAP Test2
	do
	    rm -r -f perl5/core_perl/$f
	done
    for f in App 
	do
	    rm -r -f perl5/site_perl/*/$f
	done
}

install_prepare_files() {
    #
    # prepare the GCstar files for the Win32 distribution
    #
    DEST=../dist/win32
    SRC=/mingw64	
    pwd

  	# copy GCstar 
 	 mkdir -p $DEST
  	cd ../gcstar
  	cp -a LICENSE $DEST
  	cp -a lib share $DEST
  	mkdir $DEST/bin
  	cp -a bin/gcstar bin/gcstar_test.sh bin/gcstar_test.bat $DEST/bin/
  
  	# copy Perl runtime and dependencies
  	mkdir -p $DEST/usr/bin $DEST/usr/lib $DEST/usr/share/icons
  	cp -a $SRC/bin/perl.exe $SRC/bin/wperl.exe $DEST/bin/
  	cat ../list_dll_dependencies.txt | while read f
	do
		cp -a $SRC/bin/$f $DEST/usr/bin/
	done
  	cp -a $SRC/lib/gdk-pixbuf-2.0 $SRC/lib/girepository-1.0 $SRC/lib/perl5 $DEST/usr/lib/
  	cp -a $SRC/share/glib-2.0 $DEST/usr/share/
	
	# copy icons
	cp -a $SRC/share/icons/hicolor $DEST/usr/share/icons/
	
	# copy locale for localisation
	for d in $SRC/share/locale/*
    do
		l=${d/*\//}   # find language
	    mkdir -p $DEST/usr/share/locale/$l/LC_MESSAGES
		cp -a $SRC/share/locale/$l/LC_MESSAGES/gtk30.mo $DEST/usr/share/locale/$l/LC_MESSAGES/
	done

  	# copy packaging tool
  	cp -a packages/win32/* $DEST
  	mv $DEST/*.bat $DEST/*.pl $DEST/bin

	# remove unused files
	( cd $DEST/usr/lib ; remove_unused_files )
}

install_create_installer() {
	#
	# generate GCstar installer program using NSIS
	#
	pacman -S --needed --noconfirm --noprogressbar mingw-w64-x86_64-nsis git
	cd ../dist/win32
	# append short commit sha to version number if not already done
	shortsha=$(git rev-parse --short HEAD)
	export CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$shortsha}
	sed "/my \$VERSION[^-]*;/s/';/-$CI_COMMIT_SHORT_SHA';/" < bin/gcstar > tmp.txt
	mv tmp.txt bin/gcstar
	export version=$(grep "my  *\$VERSION *=" bin/gcstar | sed "s/.*'\(.*\)';.*/\1/")
	echo "version=$version"
	makensis gcstar.nsi
	cp GCstar*.exe ../../GCstar_$version.exe
}

install_upload_installer() {
	#
	# upload installer program to the Gitlab package registry
	#
	cd ..
	f=$(echo GCstar*.exe)
	echo "version=$version"
	curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $f \
	    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/GCstar/1.7.3/$f?select=package_file"
}

echo "Installation step: $install_step"
install_$install_step 

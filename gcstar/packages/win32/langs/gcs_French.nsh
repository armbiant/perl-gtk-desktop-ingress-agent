${LSTR} LANG_CODE "FR"

${LSTR} INSTALL_APP_DEP "Défaut"
${LSTR} INSTALL_FULL "Complète"
${LSTR} INSTALL_APP_ONLY "Seulement ${PRODUCT_NAME}"

${LSTR} PRE_MULTIPLE "Le programme d'installation de ${PRODUCT_NAME} ${PRODUCT_VERSION} est déjà lancé."

${LSTR} LINK_APPLICATION "${PRODUCT_NAME}"
${LSTR} LINK_APPLICATION_DESCRIPTION "Gestionnaire de collections personnelles"
${LSTR} LINK_UPDATE "Mettre à jour les modules"
${LSTR} LINK_UPDATE_DESCRIPTION "Télécharge et installe les corrections dans les modules (ne modifie pas l'application elle-même)"
${LSTR} LINK_SITE "Site web"
${LSTR} LINK_SITE_DESCRIPTION "Visiter le site Web de ${PRODUCT_NAME}"
${LSTR} LINK_REMOVE "Désinstaller"
${LSTR} LINK_REMOVE_DESCRIPTION "Supprimer ${PRODUCT_NAME} du système"

${LSTR} FILE_DESC "Collection ${PRODUCT_NAME}"

${LSTR} DOWNLOAD_DOWNLOADING "Téléchargement de %s"
${LSTR} DOWNLOAD_CONNECTING "Connection en cours..."
${LSTR} DOWNLOAD_SECOND "seconde"
${LSTR} DOWNLOAD_MINUTE "minute"
${LSTR} DOWNLOAD_HOUR "heure"
${LSTR} DOWNLOAD_PLURAL "s"
${LSTR} DOWNLOAD_PROGRESS "%dko (%d%%) sur %dko"
${LSTR} DOWNLOAD_REMAINING "  (Temps restant : %d %s%s)"

${LSTR} PERL_VERIFYING "Verification des modules Perl disponibles..."
${LSTR} PERL_INSTALLING "Installation du module"
${LSTR} PERL_UPDATING "Mise a jour du module"
${LSTR} PERL_WAIT "Veuillez patientez, cela peut durer un certain temps..."
${LSTR} PERL_INSTALLERROR "ActivePerl n'a pas été installé.$\r$\n \
Sans ce programme ${PRODUCT_NAME} ne pourra pas fonctionner.$\r$\n \
Veuillez l'installer manuellement et relancer cette installation.$\r$\n \
Voulez-vous vous rendre sur la page de téléchargement d'ActivePerl ?"

${LSTR} SEC_DEPENDENCIES "Dépendances"
${LSTR} SEC_ACTIVEPERL "Installer ActivePerl"
${LSTR} SEC_LIB "Bibliothèques Gtk3"
${LSTR} SEC_PPM "Modules Perl"
${LSTR} SEC_UN "Supprimer version précédente"
${LSTR} SEC_ICONS "Icones"
${LSTR} SEC_DSK "Icone sur le bureau"
${LSTR} SEC_QUICK "Icone de lancement rapide"

${LSTR} DESC_DEPENDENCIES "Installer d'autres programmes utilisés par ${PRODUCT_NAME}."
${LSTR} DESC_ACTIVEPERL "Télécharger (environ 12.5 Mo) et installer ActivePerl."
${LSTR} DESC_GTKPERL "Installer les dépendances Gtk3. Si vous n'êtes pas sûr, installez les."
${LSTR} DESC_GTK "Installer les librairies Gtk3 utilisées par ${PRODUCT_NAME}."
${LSTR} DESC_PERL "Installer les liaisons pour Perl (Modules ActivePerl)."
${LSTR} DESC_UN "Supprimer la précédente installation."
${LSTR} DESC_APP "Installer les composants de ${PRODUCT_NAME} (Obligatoire)."
${LSTR} DESC_ICONS "Ajouter des icones de lancement."
${LSTR} DESC_DESKTOP "Ajoute une icone sur le bureau."
${LSTR} DESC_QUICK "Ajoute une icone dans la barre de lancement rapide (si disponible)."

${LSTR} INSTALL_USER "${PRODUCT_NAME} nécessite un changement dans l'environnement pour pouvoir fonctionner. Voulez vous que ce soit fait pour tous les utilisateurs ?$\r$\nEn répondant Non, cela sera uniquement fait pour l'utilisateur courant.$\r$\n(Des permissions administrateur sont nécessaires pour le faire pour tous les utilisateurs)"
${LSTR} INSTALL_ALL_ERROR "L'environnement ne peut être changé pour tous les utilisateurs$\r$\nVoulez vous essayer de le changer pour celui courant?"
${LSTR} INSTALL_USER_ERROR "L'environnement ne peut pas être changé pour l'utilisateur courant."

${LSTR} FINISH_LAUNCH "Lancer ${PRODUCT_NAME}"
${LSTR} FINISH_WEBSITE "Visiter le site web ${PRODUCT_NAME}"

${LSTR} UN_QUESTION "Voulez vous vraiment supprimer $(^Name) et tous ses composants ?"
${LSTR} UN_PERSO "Voulez-vous aussi supprimer vos informations personnelles (collections et configuration) ?"
${LSTR} UN_SUCESS "$(^Name) a été supprimé avec succès de votre ordinateur."
${LSTR} UN_ABORTED "La désinstallation a été arrêtée."
